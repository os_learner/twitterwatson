
def edits(s):
	s=s.lower()
	s=s.replace('you','u')
	s=s.replace('hashtag','#')
	s=s.replace('see','c')
	s=s.replace('what','wt')
	s=s.replace('one','1')
	s=s.replace('two','2')
	s=s.replace('three','3')
	s=s.replace('four','4')
	s=s.replace('five','5')
	s=s.replace('six','6')
	s=s.replace('seven','7')
	s=s.replace('eight','8')
	s=s.replace('nine','9')
	s=s.replace('zero','0')
	s=s.replace('open quote','"')
	s=s.replace('close quote','"')
	return s

def changeNumber(s):
	s=s.replace('one','1')
	s=s.replace('two','2')
	s=s.replace('three','3')
	s=s.replace('four','4')
	s=s.replace('five','5')
	s=s.replace('six','6')
	s=s.replace('seven','7')
	s=s.replace('eight','8')
	s=s.replace('nine','9')
	s=s.replace('zero','0')
	return s